//
//  FlashViewController.swift
//  KineticMission
//
//  Created by SHOKI TAKEDA on 1/10/16.
//  Copyright © 2016 kineticmission.com. All rights reserved.
//

import UIKit

class FlashViewController: UIViewController, NADViewDelegate {
    private var nadView: NADView!
    var timer:NSTimer!
    var intervalTimer:NSTimer!
    var adTimer:NSTimer!
    var counter:Double = 0
    @IBOutlet weak var obj1: UILabel!
    @IBOutlet weak var obj2: UILabel!
    @IBOutlet weak var obj3: UILabel!
    @IBOutlet weak var obj4: UILabel!
    @IBOutlet weak var obj5: UILabel!
    @IBOutlet weak var obj6: UILabel!
    @IBOutlet weak var obj7: UILabel!
    @IBOutlet weak var obj8: UILabel!
    @IBOutlet weak var obj9: UILabel!
    @IBOutlet weak var obj10: UILabel!
    @IBOutlet weak var obj11: UILabel!
    @IBOutlet weak var obj12: UILabel!
    @IBOutlet weak var obj13: UILabel!
    @IBOutlet weak var obj14: UILabel!
    @IBOutlet weak var obj15: UILabel!
    @IBOutlet weak var obj16: UILabel!
    @IBOutlet weak var obj17: UILabel!
    @IBOutlet weak var obj18: UILabel!
    @IBOutlet weak var obj19: UILabel!
    @IBOutlet weak var obj20: UILabel!
    @IBOutlet weak var obj21: UILabel!
    @IBOutlet weak var obj22: UILabel!
    @IBOutlet weak var obj23: UILabel!
    @IBOutlet weak var obj24: UILabel!
    @IBOutlet weak var obj25: UILabel!
    @IBOutlet weak var obj26: UILabel!
    @IBOutlet weak var obj27: UILabel!
    @IBOutlet weak var obj28: UILabel!
    @IBOutlet weak var obj29: UILabel!
    @IBOutlet weak var obj30: UILabel!
    @IBOutlet weak var obj31: UILabel!
    @IBOutlet weak var obj32: UILabel!
    @IBOutlet weak var obj33: UILabel!
    @IBOutlet weak var obj34: UILabel!
    @IBOutlet weak var obj35: UILabel!
    @IBOutlet weak var obj36: UILabel!
    @IBOutlet weak var obj37: UILabel!
    @IBOutlet weak var obj38: UILabel!
    @IBOutlet weak var obj39: UILabel!
    @IBOutlet weak var obj40: UILabel!
    @IBOutlet weak var obj41: UILabel!
    @IBOutlet weak var obj42: UILabel!
    @IBOutlet weak var obj43: UILabel!
    @IBOutlet weak var obj44: UILabel!
    @IBOutlet weak var obj45: UILabel!
    @IBOutlet weak var obj46: UILabel!
    @IBOutlet weak var obj47: UILabel!
    @IBOutlet weak var obj48: UILabel!
    @IBOutlet weak var obj49: UILabel!
    @IBOutlet weak var obj50: UILabel!
    @IBOutlet weak var obj51: UILabel!
    @IBOutlet weak var obj52: UILabel!
    @IBOutlet weak var obj53: UILabel!
    @IBOutlet weak var obj54: UILabel!
    @IBOutlet weak var obj55: UILabel!
    @IBOutlet weak var obj56: UILabel!
    @IBOutlet weak var obj57: UILabel!
    @IBOutlet weak var obj58: UILabel!
    @IBOutlet weak var obj59: UILabel!
    @IBOutlet weak var obj60: UILabel!
    @IBOutlet weak var obj61: UILabel!
    @IBOutlet weak var obj62: UILabel!
    @IBOutlet weak var obj63: UILabel!
    @IBOutlet weak var obj64: UILabel!
    @IBOutlet weak var obj65: UILabel!
    @IBOutlet weak var obj66: UILabel!
    @IBOutlet weak var obj67: UILabel!
    @IBOutlet weak var obj68: UILabel!
    @IBOutlet weak var obj69: UILabel!
    @IBOutlet weak var obj70: UILabel!
    @IBOutlet weak var obj71: UILabel!
    @IBOutlet weak var obj72: UILabel!
    @IBOutlet weak var obj73: UILabel!
    @IBOutlet weak var obj74: UILabel!
    @IBOutlet weak var obj75: UILabel!
    @IBOutlet weak var obj76: UILabel!
    @IBOutlet weak var obj77: UILabel!
    @IBOutlet weak var obj78: UILabel!
    @IBOutlet weak var obj79: UILabel!
    @IBOutlet weak var obj80: UILabel!
    @IBOutlet weak var obj81: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        if (timer != nil) {
            timer.invalidate()
        }
        if (intervalTimer != nil) {
            intervalTimer.invalidate()
        }
        adTimer = NSTimer.scheduledTimerWithTimeInterval(6.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        self.view.bringSubviewToFront(nadView)
        
        intervalTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("onIntervalUpdate"), userInfo: nil, repeats: true)
    }
    
    func onUpdate(){
        if (timer != nil) {
            timer.invalidate()
        }
        if (intervalTimer != nil) {
            intervalTimer.invalidate()
        }
        obj1.text = "▼"
        obj2.text = "▼"
        obj3.text = "▼"
        obj4.text = "▼"
        obj5.text = "▼"
        obj6.text = "▼"
        obj7.text = "▼"
        obj8.text = "▼"
        obj9.text = "▼"
        obj10.text = "▼"
        obj11.text = "▼"
        obj12.text = "▼"
        obj13.text = "▼"
        obj14.text = "▼"
        obj15.text = "▼"
        obj16.text = "▼"
        obj17.text = "▼"
        obj18.text = "▼"
        obj19.text = "▼"
        obj20.text = "▼"
        obj21.text = "▼"
        obj22.text = "▼"
        obj23.text = "▼"
        obj24.text = "▼"
        obj25.text = "▼"
        obj26.text = "▼"
        obj27.text = "▼"
        obj28.text = "▼"
        obj29.text = "▼"
        obj30.text = "▼"
        obj31.text = "▼"
        obj32.text = "▼"
        obj33.text = "▼"
        obj34.text = "▼"
        obj35.text = "▼"
        obj36.text = "▼"
        obj37.text = "▼"
        obj38.text = "▼"
        obj39.text = "▼"
        obj40.text = "▼"
        obj41.text = "▼"
        obj42.text = "▼"
        obj43.text = "▼"
        obj44.text = "▼"
        obj45.text = "▼"
        obj46.text = "▼"
        obj47.text = "▼"
        obj48.text = "▼"
        obj49.text = "▼"
        obj50.text = "▼"
        obj51.text = "▼"
        obj52.text = "▼"
        obj53.text = "▼"
        obj54.text = "▼"
        obj55.text = "▼"
        obj56.text = "▼"
        obj57.text = "▼"
        obj58.text = "▼"
        obj59.text = "▼"
        obj60.text = "▼"
        obj61.text = "▼"
        obj62.text = "▼"
        obj63.text = "▼"
        obj64.text = "▼"
        obj65.text = "▼"
        obj66.text = "▼"
        obj67.text = "▼"
        obj68.text = "▼"
        obj69.text = "▼"
        obj70.text = "▼"
        obj71.text = "▼"
        obj72.text = "▼"
        obj73.text = "▼"
        obj74.text = "▼"
        obj75.text = "▼"
        obj76.text = "▼"
        obj77.text = "▼"
        obj78.text = "▼"
        obj79.text = "▼"
        obj80.text = "▼"
        obj81.text = "▼"
        obj1.hidden = true
        obj2.hidden = true
        obj3.hidden = true
        obj4.hidden = true
        obj5.hidden = true
        obj6.hidden = true
        obj7.hidden = true
        obj8.hidden = true
        obj9.hidden = true
        obj10.hidden = true
        obj11.hidden = true
        obj12.hidden = true
        obj13.hidden = true
        obj14.hidden = true
        obj15.hidden = true
        obj16.hidden = true
        obj17.hidden = true
        obj18.hidden = true
        obj19.hidden = true
        obj20.hidden = true
        obj21.hidden = true
        obj22.hidden = true
        obj23.hidden = true
        obj24.hidden = true
        obj25.hidden = true
        obj26.hidden = true
        obj27.hidden = true
        obj28.hidden = true
        obj29.hidden = true
        obj30.hidden = true
        obj31.hidden = true
        obj32.hidden = true
        obj33.hidden = true
        obj34.hidden = true
        obj35.hidden = true
        obj36.hidden = true
        obj37.hidden = true
        obj38.hidden = true
        obj39.hidden = true
        obj40.hidden = true
        obj41.hidden = true
        obj42.hidden = true
        obj43.hidden = true
        obj44.hidden = true
        obj45.hidden = true
        obj46.hidden = true
        obj47.hidden = true
        obj48.hidden = true
        obj49.hidden = true
        obj50.hidden = true
        obj51.hidden = true
        obj52.hidden = true
        obj53.hidden = true
        obj54.hidden = true
        obj55.hidden = true
        obj56.hidden = true
        obj57.hidden = true
        obj58.hidden = true
        obj59.hidden = true
        obj60.hidden = true
        obj61.hidden = true
        obj62.hidden = true
        obj63.hidden = true
        obj64.hidden = true
        obj65.hidden = true
        obj66.hidden = true
        obj67.hidden = true
        obj68.hidden = true
        obj69.hidden = true
        obj70.hidden = true
        obj71.hidden = true
        obj72.hidden = true
        obj73.hidden = true
        obj74.hidden = true
        obj75.hidden = true
        obj76.hidden = true
        obj77.hidden = true
        obj78.hidden = true
        obj79.hidden = true
        obj80.hidden = true
        obj81.hidden = true
        
        intervalTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("onIntervalUpdate"), userInfo: nil, repeats: true)
    }
    
    func onIntervalUpdate() {
        if (timer != nil) {
            timer.invalidate()
        }
        if (intervalTimer != nil) {
            intervalTimer.invalidate()
        }
        let random1 = Int(arc4random_uniform(UInt32(4)))
        if random1 == 0 || random1 == 1 {
            obj1.hidden = false
        }
        let random2 = Int(arc4random_uniform(UInt32(4)))
        if random2 == 0 || random2 == 1 {
            obj2.hidden = false
        }
        let random3 = Int(arc4random_uniform(UInt32(4)))
        if random3 == 0 || random3 == 1 {
            obj3.hidden = false
        }
        let random4 = Int(arc4random_uniform(UInt32(4)))
        if random4 == 0 || random4 == 1 {
            obj4.hidden = false
        }
        let random5 = Int(arc4random_uniform(UInt32(4)))
        if random5 == 0 || random5 == 1 {
            obj5.hidden = false
        }
        let random6 = Int(arc4random_uniform(UInt32(4)))
        if random6 == 0 || random6 == 1 {
            obj6.hidden = false
        }
        let random7 = Int(arc4random_uniform(UInt32(4)))
        if random7 == 0 || random7 == 1 {
            obj7.hidden = false
        }
        let random8 = Int(arc4random_uniform(UInt32(4)))
        if random8 == 0 || random8 == 1 {
            obj8.hidden = false
        }
        let random9 = Int(arc4random_uniform(UInt32(4)))
        if random9 == 0 || random9 == 1 {
            obj9.hidden = false
        }
        let random10 = Int(arc4random_uniform(UInt32(4)))
        if random10 == 0 || random10 == 1 {
            obj10.hidden = false
        }
        let random11 = Int(arc4random_uniform(UInt32(4)))
        if random11 == 0 || random11 == 1 {
            obj11.hidden = false
        }
        let random12 = Int(arc4random_uniform(UInt32(4)))
        if random12 == 0 || random12 == 1 {
            obj12.hidden = false
        }
        let random13 = Int(arc4random_uniform(UInt32(4)))
        if random13 == 0 || random13 == 1 {
            obj13.hidden = false
        }
        let random14 = Int(arc4random_uniform(UInt32(4)))
        if random14 == 0 || random14 == 1 {
            obj14.hidden = false
        }
        let random15 = Int(arc4random_uniform(UInt32(4)))
        if random15 == 0 || random15 == 1 {
            obj15.hidden = false
        }
        let random16 = Int(arc4random_uniform(UInt32(4)))
        if random16 == 0 || random16 == 1 {
            obj16.hidden = false
        }
        let random17 = Int(arc4random_uniform(UInt32(4)))
        if random17 == 0 || random17 == 1 {
            obj17.hidden = false
        }
        let random18 = Int(arc4random_uniform(UInt32(4)))
        if random18 == 0 || random18 == 1 {
            obj18.hidden = false
        }
        let random19 = Int(arc4random_uniform(UInt32(4)))
        if random19 == 0 || random19 == 1 {
            obj19.hidden = false
        }
        let random20 = Int(arc4random_uniform(UInt32(4)))
        if random20 == 0 || random20 == 1 {
            obj20.hidden = false
        }
        let random21 = Int(arc4random_uniform(UInt32(4)))
        if random21 == 0 || random21 == 1 {
            obj21.hidden = false
        }
        let random22 = Int(arc4random_uniform(UInt32(4)))
        if random22 == 0 || random22 == 1 {
            obj22.hidden = false
        }
        let random23 = Int(arc4random_uniform(UInt32(4)))
        if random23 == 0 || random23 == 1 {
            obj23.hidden = false
        }
        let random24 = Int(arc4random_uniform(UInt32(4)))
        if random24 == 0 || random24 == 1 {
            obj24.hidden = false
        }
        let random25 = Int(arc4random_uniform(UInt32(4)))
        if random25 == 0 || random25 == 1 {
            obj25.hidden = false
        }
        let random26 = Int(arc4random_uniform(UInt32(4)))
        if random26 == 0 || random26 == 1 {
            obj26.hidden = false
        }
        let random27 = Int(arc4random_uniform(UInt32(4)))
        if random27 == 0 || random27 == 1 {
            obj27.hidden = false
        }
        let random28 = Int(arc4random_uniform(UInt32(4)))
        if random28 == 0 || random28 == 1 {
            obj28.hidden = false
        }
        let random29 = Int(arc4random_uniform(UInt32(4)))
        if random29 == 0 || random29 == 1 {
            obj29.hidden = false
        }
        let random30 = Int(arc4random_uniform(UInt32(4)))
        if random30 == 0 || random30 == 1 {
            obj30.hidden = false
        }
        let random31 = Int(arc4random_uniform(UInt32(4)))
        if random31 == 0 || random31 == 1 {
            obj31.hidden = false
        }
        let random32 = Int(arc4random_uniform(UInt32(4)))
        if random32 == 0 || random32 == 1 {
            obj32.hidden = false
        }
        let random33 = Int(arc4random_uniform(UInt32(4)))
        if random33 == 0 || random33 == 1 {
            obj33.hidden = false
        }
        let random34 = Int(arc4random_uniform(UInt32(4)))
        if random34 == 0 || random34 == 1 {
            obj34.hidden = false
        }
        let random35 = Int(arc4random_uniform(UInt32(4)))
        if random35 == 0 || random35 == 1 {
            obj35.hidden = false
        }
        let random36 = Int(arc4random_uniform(UInt32(4)))
        if random36 == 0 || random36 == 1 {
            obj36.hidden = false
        }
        let random37 = Int(arc4random_uniform(UInt32(4)))
        if random37 == 0 || random37 == 1 {
            obj37.hidden = false
        }
        let random38 = Int(arc4random_uniform(UInt32(4)))
        if random38 == 0 || random38 == 1 {
            obj38.hidden = false
        }
        let random39 = Int(arc4random_uniform(UInt32(4)))
        if random39 == 0 || random39 == 1 {
            obj39.hidden = false
        }
        let random40 = Int(arc4random_uniform(UInt32(4)))
        if random40 == 0 || random40 == 1 {
            obj40.hidden = false
        }
        let random41 = Int(arc4random_uniform(UInt32(4)))
        if random41 == 0 || random41 == 1 {
            obj41.hidden = false
        }
        let random42 = Int(arc4random_uniform(UInt32(4)))
        if random42 == 0 || random42 == 1 {
            obj42.hidden = false
        }
        let random43 = Int(arc4random_uniform(UInt32(4)))
        if random43 == 0 || random43 == 1 {
            obj43.hidden = false
        }
        let random44 = Int(arc4random_uniform(UInt32(4)))
        if random44 == 0 || random44 == 1 {
            obj44.hidden = false
        }
        let random45 = Int(arc4random_uniform(UInt32(4)))
        if random45 == 0 || random45 == 1 {
            obj45.hidden = false
        }
        let random46 = Int(arc4random_uniform(UInt32(4)))
        if random46 == 0 || random46 == 1 {
            obj46.hidden = false
        }
        let random47 = Int(arc4random_uniform(UInt32(4)))
        if random47 == 0 || random47 == 1 {
            obj47.hidden = false
        }
        let random48 = Int(arc4random_uniform(UInt32(4)))
        if random48 == 0 || random48 == 1 {
            obj48.hidden = false
        }
        let random49 = Int(arc4random_uniform(UInt32(4)))
        if random49 == 0 || random49 == 1 {
            obj49.hidden = false
        }
        let random50 = Int(arc4random_uniform(UInt32(4)))
        if random50 == 0 || random50 == 1 {
            obj50.hidden = false
        }
        let random51 = Int(arc4random_uniform(UInt32(4)))
        if random51 == 0 || random51 == 1 {
            obj51.hidden = false
        }
        let random52 = Int(arc4random_uniform(UInt32(4)))
        if random52 == 0 || random52 == 1 {
            obj52.hidden = false
        }
        let random53 = Int(arc4random_uniform(UInt32(4)))
        if random53 == 0 || random53 == 1 {
            obj53.hidden = false
        }
        let random54 = Int(arc4random_uniform(UInt32(4)))
        if random54 == 0 || random54 == 1 {
            obj54.hidden = false
        }
        let random55 = Int(arc4random_uniform(UInt32(4)))
        if random55 == 0 || random55 == 1 {
            obj55.hidden = false
        }
        let random56 = Int(arc4random_uniform(UInt32(4)))
        if random56 == 0 || random56 == 1 {
            obj56.hidden = false
        }
        let random57 = Int(arc4random_uniform(UInt32(4)))
        if random57 == 0 || random57 == 1 {
            obj57.hidden = false
        }
        let random58 = Int(arc4random_uniform(UInt32(4)))
        if random58 == 0 || random58 == 1 {
            obj58.hidden = false
        }
        let random59 = Int(arc4random_uniform(UInt32(4)))
        if random59 == 0 || random59 == 1 {
            obj59.hidden = false
        }
        let random60 = Int(arc4random_uniform(UInt32(4)))
        if random60 == 0 || random60 == 1 {
            obj60.hidden = false
        }
        let random61 = Int(arc4random_uniform(UInt32(4)))
        if random61 == 0 || random61 == 1 {
            obj61.hidden = false
        }
        let random62 = Int(arc4random_uniform(UInt32(4)))
        if random62 == 0 || random62 == 1 {
            obj62.hidden = false
        }
        let random63 = Int(arc4random_uniform(UInt32(4)))
        if random63 == 0 || random63 == 1 {
            obj63.hidden = false
        }
        let random64 = Int(arc4random_uniform(UInt32(4)))
        if random64 == 0 || random64 == 1 {
            obj64.hidden = false
        }
        let random65 = Int(arc4random_uniform(UInt32(4)))
        if random65 == 0 || random65 == 1 {
            obj65.hidden = false
        }
        let random66 = Int(arc4random_uniform(UInt32(4)))
        if random66 == 0 || random66 == 1 {
            obj66.hidden = false
        }
        let random67 = Int(arc4random_uniform(UInt32(4)))
        if random67 == 0 || random67 == 1 {
            obj67.hidden = false
        }
        let random68 = Int(arc4random_uniform(UInt32(4)))
        if random68 == 0 || random68 == 1 {
            obj68.hidden = false
        }
        let random69 = Int(arc4random_uniform(UInt32(4)))
        if random69 == 0 || random69 == 1 {
            obj69.hidden = false
        }
        let random70 = Int(arc4random_uniform(UInt32(4)))
        if random70 == 0 || random70 == 1 {
            obj70.hidden = false
        }
        let random71 = Int(arc4random_uniform(UInt32(4)))
        if random71 == 0 || random71 == 1 {
            obj71.hidden = false
        }
        let random72 = Int(arc4random_uniform(UInt32(4)))
        if random72 == 0 || random72 == 1 {
            obj72.hidden = false
        }
        let random73 = Int(arc4random_uniform(UInt32(4)))
        if random73 == 0 || random73 == 1 {
            obj73.hidden = false
        }
        let random74 = Int(arc4random_uniform(UInt32(4)))
        if random74 == 0 || random74 == 1 {
            obj74.hidden = false
        }
        let random75 = Int(arc4random_uniform(UInt32(4)))
        if random75 == 0 || random75 == 1 {
            obj75.hidden = false
        }
        let random76 = Int(arc4random_uniform(UInt32(4)))
        if random76 == 0 || random76 == 1 {
            obj76.hidden = false
        }
        let random77 = Int(arc4random_uniform(UInt32(4)))
        if random77 == 0 || random77 == 1 {
            obj77.hidden = false
        }
        let random78 = Int(arc4random_uniform(UInt32(4)))
        if random78 == 0 || random78 == 1 {
            obj78.hidden = false
        }
        let random79 = Int(arc4random_uniform(UInt32(4)))
        if random79 == 0 || random79 == 1 {
            obj79.hidden = false
        }
        let random80 = Int(arc4random_uniform(UInt32(4)))
        if random80 == 0 || random80 == 1 {
            obj80.hidden = false
        }
        let random81 = Int(arc4random_uniform(UInt32(4)))
        if random81 == 0 || random81 == 1 {
            obj81.hidden = false
        }
        let starRandom = Int(arc4random_uniform(UInt32(82)))
        switch starRandom {
        case 1:
            obj1.hidden = false
            obj1.text = "⭐︎"
        case 2:
            obj2.hidden = false
            obj2.text = "⭐︎"
        case 3:
            obj3.hidden = false
            obj3.text = "⭐︎"
        case 4:
            obj4.hidden = false
            obj4.text = "⭐︎"
        case 5:
            obj5.hidden = false
            obj5.text = "⭐︎"
        case 6:
            obj6.hidden = false
            obj6.text = "⭐︎"
        case 7:
            obj7.hidden = false
            obj7.text = "⭐︎"
        case 8:
            obj8.hidden = false
            obj8.text = "⭐︎"
        case 9:
            obj9.hidden = false
            obj9.text = "⭐︎"
        case 10:
            obj10.hidden = false
            obj10.text = "⭐︎"
        case 11:
            obj11.hidden = false
            obj11.text = "⭐︎"
        case 12:
            obj12.hidden = false
            obj12.text = "⭐︎"
        case 13:
            obj13.hidden = false
            obj13.text = "⭐︎"
        case 14:
            obj14.hidden = false
            obj14.text = "⭐︎"
        case 15:
            obj15.hidden = false
            obj15.text = "⭐︎"
        case 16:
            obj16.hidden = false
            obj16.text = "⭐︎"
        case 17:
            obj17.hidden = false
            obj17.text = "⭐︎"
        case 18:
            obj18.hidden = false
            obj18.text = "⭐︎"
        case 19:
            obj19.hidden = false
            obj19.text = "⭐︎"
        case 20:
            obj20.hidden = false
            obj20.text = "⭐︎"
        case 21:
            obj21.hidden = false
            obj21.text = "⭐︎"
        case 22:
            obj22.hidden = false
            obj22.text = "⭐︎"
        case 23:
            obj23.hidden = false
            obj23.text = "⭐︎"
        case 24:
            obj24.hidden = false
            obj24.text = "⭐︎"
        case 25:
            obj25.hidden = false
            obj25.text = "⭐︎"
        case 26:
            obj26.hidden = false
            obj26.text = "⭐︎"
        case 27:
            obj27.hidden = false
            obj27.text = "⭐︎"
        case 28:
            obj28.hidden = false
            obj28.text = "⭐︎"
        case 29:
            obj29.hidden = false
            obj29.text = "⭐︎"
        case 30:
            obj30.hidden = false
            obj30.text = "⭐︎"
        case 31:
            obj31.hidden = false
            obj31.text = "⭐︎"
        case 32:
            obj32.hidden = false
            obj32.text = "⭐︎"
        case 33:
            obj33.hidden = false
            obj33.text = "⭐︎"
        case 34:
            obj34.hidden = false
            obj34.text = "⭐︎"
        case 35:
            obj35.hidden = false
            obj35.text = "⭐︎"
        case 36:
            obj36.hidden = false
            obj36.text = "⭐︎"
        case 37:
            obj37.hidden = false
            obj37.text = "⭐︎"
        case 38:
            obj38.hidden = false
            obj38.text = "⭐︎"
        case 39:
            obj39.hidden = false
            obj39.text = "⭐︎"
        case 40:
            obj40.hidden = false
            obj40.text = "⭐︎"
        case 41:
            obj41.hidden = false
            obj41.text = "⭐︎"
        case 42:
            obj42.hidden = false
            obj42.text = "⭐︎"
        case 43:
            obj43.hidden = false
            obj43.text = "⭐︎"
        case 44:
            obj44.hidden = false
            obj44.text = "⭐︎"
        case 45:
            obj45.hidden = false
            obj45.text = "⭐︎"
        case 46:
            obj46.hidden = false
            obj46.text = "⭐︎"
        case 47:
            obj47.hidden = false
            obj47.text = "⭐︎"
        case 48:
            obj48.hidden = false
            obj48.text = "⭐︎"
        case 49:
            obj49.hidden = false
            obj49.text = "⭐︎"
        case 50:
            obj50.hidden = false
            obj50.text = "⭐︎"
        case 51:
            obj51.hidden = false
            obj51.text = "⭐︎"
        case 52:
            obj52.hidden = false
            obj52.text = "⭐︎"
        case 53:
            obj53.hidden = false
            obj53.text = "⭐︎"
        case 54:
            obj54.hidden = false
            obj54.text = "⭐︎"
        case 55:
            obj55.hidden = false
            obj55.text = "⭐︎"
        case 56:
            obj56.hidden = false
            obj56.text = "⭐︎"
        case 57:
            obj57.hidden = false
            obj57.text = "⭐︎"
        case 58:
            obj58.hidden = false
            obj58.text = "⭐︎"
        case 59:
            obj59.hidden = false
            obj59.text = "⭐︎"
        case 60:
            obj60.hidden = false
            obj60.text = "⭐︎"
        case 61:
            obj61.hidden = false
            obj61.text = "⭐︎"
        case 62:
            obj62.hidden = false
            obj62.text = "⭐︎"
        case 63:
            obj63.hidden = false
            obj63.text = "⭐︎"
        case 64:
            obj64.hidden = false
            obj64.text = "⭐︎"
        case 65:
            obj65.hidden = false
            obj65.text = "⭐︎"
        case 66:
            obj66.hidden = false
            obj66.text = "⭐︎"
        case 67:
            obj67.hidden = false
            obj67.text = "⭐︎"
        case 68:
            obj68.hidden = false
            obj68.text = "⭐︎"
        case 69:
            obj69.hidden = false
            obj69.text = "⭐︎"
        case 70:
            obj70.hidden = false
            obj70.text = "⭐︎"
        case 71:
            obj71.hidden = false
            obj71.text = "⭐︎"
        case 72:
            obj72.hidden = false
            obj72.text = "⭐︎"
        case 73:
            obj73.hidden = false
            obj73.text = "⭐︎"
        case 74:
            obj74.hidden = false
            obj74.text = "⭐︎"
        case 75:
            obj75.hidden = false
            obj75.text = "⭐︎"
        case 76:
            obj76.hidden = false
            obj76.text = "⭐︎"
        case 77:
            obj77.hidden = false
            obj77.text = "⭐︎"
        case 78:
            obj78.hidden = false
            obj78.text = "⭐︎"
        case 79:
            obj79.hidden = false
            obj79.text = "⭐︎"
        case 80:
            obj80.hidden = false
            obj80.text = "⭐︎"
        case 81:
            obj81.hidden = false
            obj81.text = "⭐︎"
        default: break
        }
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("onUpdate"), userInfo: nil, repeats: true)
    }
    
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
