//
//  GameScene.swift
//  KineticMission
//
//  Created by SHOKI TAKEDA on 1/10/16.
//  Copyright (c) 2016 kineticmission.com. All rights reserved.
//

import SpriteKit

extension SKScene{
    func GetMidPoint()->CGPoint{
        return CGPointMake(self.frame.midX, self.frame.midY)
    }
}

class GameScene: SKScene {
    var timer:NSTimer!
    var redcircle : SKShapeNode!
    var counter:Int = 1
    var background = SKSpriteNode(imageNamed: "bgSky")
    override func didMoveToView(view: SKView) {
        self.size.width = view.bounds.size.width
        self.size.height = view.bounds.size.height
        self.physicsWorld.gravity = CGVectorMake(0.0, 0.0)
        self.physicsBody = SKPhysicsBody(edgeLoopFromRect: self.frame)

        background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        background.size = self.frame.size
        background.zPosition = -1
        addChild(background)
        
        redcircle = SKShapeNode(circleOfRadius: 15.0)
        redcircle.fillColor = UIColor.whiteColor()
        redcircle.position = self.GetMidPoint()
        redcircle.physicsBody = SKPhysicsBody(circleOfRadius: 15.0)
        redcircle.physicsBody?.velocity = CGVectorMake(5000.0, 5000.0)
        redcircle.physicsBody?.friction = 0.0
        
        self.addChild(redcircle)
        
        timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: Selector("onUpdate"), userInfo: nil, repeats: true)
    }
    
    func onUpdate(){
        let random = Int(arc4random_uniform(UInt32(4)))
        switch random {
        case 0:
            redcircle.physicsBody?.velocity = CGVectorMake(5000.0, 5000.0)
        case 1:
            redcircle.physicsBody?.velocity = CGVectorMake(-5000.0, 5000.0)
        case 2:
            redcircle.physicsBody?.velocity = CGVectorMake(5000.0, -5000.0)
        case 3:
            redcircle.physicsBody?.velocity = CGVectorMake(-5000.0, -5000.0)
        default:break
        }
    }
}