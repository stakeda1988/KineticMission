//
//  GameScene.swift
//  Test
//
//  Created by SHOKI TAKEDA on 1/30/16.
//  Copyright (c) 2016 kineticmission.com. All rights reserved.
//

import SpriteKit

class TestScene: SKScene {
    var speedShowTimer:NSTimer!
    var moveTimer:NSTimer!
    var answerTimer:NSTimer!
    var redcircle : SKShapeNode!
    var redlabel : SKLabelNode!
    var showlabel : SKLabelNode!
    var counter:Int = 1
    var background = SKSpriteNode(imageNamed: "bgSky")
    var globalRandom:Int = 0
    var globalSpeed:Int = 0
    var speedCounter:Int = 0
    override func didMoveToView(view: SKView) {
        self.physicsWorld.gravity = CGVectorMake(0.0, 0.0)
        if (speedShowTimer != nil) {
            speedShowTimer.invalidate()
        }
        if (moveTimer != nil) {
            moveTimer.invalidate()
        }
        if (answerTimer != nil) {
            answerTimer.invalidate()
        }
        
        background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        background.size = self.frame.size
        background.zPosition = -1
        addChild(background)
        
        speedShowTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("onSpeedShow"), userInfo: nil, repeats: true)
    }
    
    func onSpeedShow(){
        if (speedShowTimer != nil) {
            speedShowTimer.invalidate()
        }
        if (moveTimer != nil) {
            moveTimer.invalidate()
        }
        if (answerTimer != nil) {
            answerTimer.invalidate()
        }
        self.removeAllChildren()
        
        background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        background.size = self.frame.size
        background.zPosition = -1
        addChild(background)
        
        if speedCounter > 4{
            speedCounter = 0
        }
        showlabel = SKLabelNode(fontNamed:"GurmukhiMN")
        switch speedCounter {
        case 0:
            globalSpeed = 100
        case 1:
            globalSpeed = 200
        case 2:
            globalSpeed = 500
        case 3:
            globalSpeed = 1000
        case 4:
            globalSpeed = 10000
        default:break
        }
        showlabel.text = "Speed : " + String(globalSpeed) + " km/h"
        showlabel.fontSize = 40
        showlabel.position = CGPointMake(self.frame.midX, self.frame.midY-70)
        showlabel.physicsBody = SKPhysicsBody(circleOfRadius: 15.0)
        showlabel.physicsBody?.velocity = CGVectorMake(0, 0)
        showlabel.physicsBody?.friction = 0.0
        self.addChild(showlabel)
        
        speedCounter++
        
        moveTimer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("onMove"), userInfo: nil, repeats: true)
    }
    
    func onMove(){
        if (speedShowTimer != nil) {
            speedShowTimer.invalidate()
        }
        if (moveTimer != nil) {
            moveTimer.invalidate()
        }
        if (answerTimer != nil) {
            answerTimer.invalidate()
        }
        self.removeAllChildren()
        
        background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        background.size = self.frame.size
        background.zPosition = -1
        addChild(background)
        
        redlabel = SKLabelNode(fontNamed:"GurmukhiMN")
        let random = Int(arc4random_uniform(100))
        globalRandom = random
        redlabel.text = String(random)
        redlabel.fontSize = 40
        redlabel.position = CGPointMake(self.frame.maxX, self.frame.midY)
        redlabel.physicsBody = SKPhysicsBody(circleOfRadius: 15.0)
        redlabel.physicsBody?.velocity = CGVectorMake(CGFloat(-(globalSpeed*10)), 0)
        
        redlabel.physicsBody?.friction = 0.0
        self.addChild(redlabel)
        
        answerTimer = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: Selector("onAnswer"), userInfo: nil, repeats: true)
    }
    
    func onAnswer(){
        if (speedShowTimer != nil) {
            speedShowTimer.invalidate()
        }
        if (moveTimer != nil) {
            moveTimer.invalidate()
        }
        if (answerTimer != nil) {
            answerTimer.invalidate()
        }
        self.removeAllChildren()
        
        background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        background.size = self.frame.size
        background.zPosition = -1
        addChild(background)
        
        showlabel = SKLabelNode(fontNamed:"GurmukhiMN")
        showlabel.text = "Answer : " + String(globalRandom)
        showlabel.fontSize = 40
        showlabel.position = CGPointMake(self.frame.midX, self.frame.midY-70)
        showlabel.physicsBody = SKPhysicsBody(circleOfRadius: 15.0)
        showlabel.physicsBody?.velocity = CGVectorMake(0, 0)
        showlabel.physicsBody?.friction = 0.0
        self.addChild(showlabel)
        
        speedShowTimer = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: Selector("onSpeedShow"), userInfo: nil, repeats: true)
    }
}
